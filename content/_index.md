### Internal Family Systems  

# IFS-life

IFS voor proffessionals, als levensstijl en als bron van inspiratie.



#### Agenda:
```
- 16 April 2023 oefendag (ISIL/Level_1) 
- 28 Mei 2023 FamilyDag  verschoven naar het najaar (waarschijnlijk aan de Lek)

```

#### Therapeuten

Vindt een [IFS therapeut in Nederland ISIL of level_1/2/3)](http://ifsinnederland.nl/)

#### Trainingen in Nederland

Er zijn nog geen formele IFS-trainingen in nederland maar hieronder kun je inspiratie op doen:
- [Innerlijk Stemmen Innerlijke Leiding](https://www.indesmeltkroes.nl/innerlijke-stemmen-innerlijke-leiding.html)

- [Introductie / Relatie](https://www.internalfamilysystemstherapeut.nl/IFStraining)
- [IFS-mastermind](https://seekdeeply.com/)
- [Karibucoaching](https://www.karibucoaching.nl/training/)

#### Social Networks 

-  Voor mensen die minimaal level_1 of [ISIL](https://www.indesmeltkroes.nl/innerlijke-stemmen-innerlijke-leiding.html) gedaan hebben kunnen elkaar vinden via [Signal](https://signal.group/#CjQKIEw0jZG6KHPD7QN7K2gZpuujh2ApCjZlmI1vJcxAdiyuEhDfPIWuDmOCJb-AzMOljWT9)
- Intervisie Level_1 start in zomer 2023 via [Signal](https://signal.group/#CjQKIIKoQo9gry3Vvo5Nso_qm_qLm3tVRB8CFITMbRVGKcamEhAOHO4sIc8UGNaKzq9L602b)
- Oefendagen [Nina en Jeannine](https://www.ifsoefenmiddag.nl/)
- Oefendagen ISIL/Level_1 met Isabelle en Marten via [Signal](https://signal.group/#CjQKILNCAZrbk2HtyPyDbbTYzTYsaXUGUFrpqWuReNEkzljwEhBOOfj6h8A6DtMLNUgoDTmf)




#### Social Media

- [FaceBook groep](https://www.facebook.com/groups/728735504376650)
- [Linkedin](https://www.linkedin.com/groups/12737196/)



#### The bigger picture

![mindmap_2ct](map.png)

