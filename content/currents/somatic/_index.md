---
title: Somatic
weight: 2
---

# Somatic IFS

[Susan McConnell ](https://www.embodiedself.net/about-somatic-ifs)

vijf practices in embodied Self:

```
awareness
breath
radical resonance
movement
touch
```

[boek](https://www.bol.com/nl/nl/p/somatic-internal-family-systems-therapy/9200000127601130/?referrer=socialshare_pdp_www)

