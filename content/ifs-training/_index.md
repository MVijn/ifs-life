+++
title = "IFS training"
menuTitle = "IFS-training"
+++

### Level 1

Algemene Informatie over level 1 trainingen kun je vinden op de IFS [site](https://ifs-institute.com/trainings/level-1)

Bij de volgende andere organisaties is het mogelijk
een formele Level 1 training te doen. Momenteel is het niet zo gemakkelijk om daar tussen te komen:

- [US-international](https://ifs-institute.com/trainings/level-1/international-trainings)
- [US/CAN-National](https://ifs-institute.com/trainings/level-1/north-american-trainings)
- [UK](https://internalfamilysystemstraining.co.uk/training/level-1)
- [Frankrijk](https://ifs-association.com)
- [Portugal](https://internalfamilysystems.pt)
- [Life architect (Polen/UK)](https://lifearchitect.com)
- [Israel](https://www.ifs-israel.org/training-info-en)
- [Spain](https://institutoifs.com/level-i-training-of-internal-family-systems-ifs/)
- als je er nog meer weet hoor ik het graag....

### email nofitications
 
* [US](https://ifs-institute.com/trainings/email-notifications)
* Er zijn er meer maar niet altijd even makkelijk te vinden... 

 
