---
title:  IFS Daily 
weight: 2
---


# suggesties voor dagelijkse beoefingen:

- meditatie 
- kampvuur met je delen
- Inquiry met een partner
- Linking met een partner
- Ademwerk
- Solo-IFs van [Lucille Wayne](https://seekdeeply.com/)



## Inquiry

Kan live of online in een tweetal. Korte meditatie en dan
om en om ieder 15 minuten open onderzoek naar je delen
Korter of langer kan natuurlijk ook.


## Linking

Kan live of online in een tweetal. Korte meditatie en dan steeds kort beschrijven
waar ben ik nu, welke delen zijn actief. Stilte en de ander beschrijft kort wat is er nu, welke delen zijn (re-)aktief. Als je veel Self is kan je ook
nieuwsgierigheid uiten naar delen van de ander, maar die hoeft daar niet perse op te reageren. Dat wissel je een gedurende 30 minuten af. 

## Ademwerk

Zoek eerst ook even uit of ademwerk iets voor je is. Lees over contra-indicaties, medicijnen en mogelijke gevolgen voor je gestel.
 
Bijvoolbeeld meng [Kath-mediatie](https://www.diamondapproach.org/glossary/refinery_phrases/kath), [Wim Hoff](https://www.wimhofmethod.com/) en [CotH](https://www.codesoftheheart.com/) tot het volgende:




### 3-4 Rondes:
- 2 minuten 2:1 :: lange inademing, kort uit (poef, zonder druk) gelijkmatige verdeling over buik,ribben en (hoge) borst. Een keer live oefenen bij Lo helpt zeker, en een enkele keer doen we het ook op de ISIL oefendagen.  

- max 2 minuten  retentie/inhouden : adem uit en hou vast, tot je weer adem nodig hebt, iedereen
verschilt hier en persen,harden werken is niet nodig. Hier lig je op een veilige plek. Dus ben je niet in het water of op fiets etc etc. 
Sommige doen hier korter en een enkeling langer. Hier doe ik zelf kath-meditatie.

- 15-20  seconden adem in en houd vast. Zet lichte druk op het midden van je hoofd, de pijn appelklier. Bij mij komt er Self/energie vrij die naar plaatsen of delen kan brengen die wat liefde en aandacht kunnen gebruiken. 


Dit rondje herhaal je 3 of 4 keer voor je opstaat....

Dan is het wellicht fijn om wat na te liggen en de fases/stappen van
Somatic-IFS te doen:

- aandacht
- adem
- radicale resonance
- beweging
- aanraking 



