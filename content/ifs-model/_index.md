+++
title = "IFS Model"
menuTitle = "IFS-model"
+++

### snippets of the model



{{<mermaid align="left">}}
flowchart TD
ifs_model --> 6Fs
ifs_model --> 5Ps
ifs_model --> 8Cs
ifs_model --> unburdening
{{< /mermaid >}}
