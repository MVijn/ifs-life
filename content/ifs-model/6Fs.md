---
title: "6f's"
weight: 1
---



### schema of the 6 F's

{{<mermaid align="left">}}
flowchart TD

Find --> Focus
Focus --> Flesh_Out
Flesh_Out --> check{"How are you feeling towards the part?" }
check -- no_Self_Energy --> Focus
check -- Self_Energy --> BeFriend
BeFriend --> WhatistheFear?
{{< /mermaid >}}
